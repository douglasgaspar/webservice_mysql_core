﻿namespace webservicemysql
{
    public class Produto
    {
        public int id { get; set; }
        public String nome { get; set; }
        public double preco { get; set; }
        public int quantidade { get; set; }

        public Produto() { }
        public Produto(int id, string nome, double preco, int quantidade)
        {
            this.id = id;
            this.nome = nome;
            this.preco = preco;
            this.quantidade = quantidade;
        }
    }
}
