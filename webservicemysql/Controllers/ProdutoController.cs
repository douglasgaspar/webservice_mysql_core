﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

namespace webservicemysql.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        // GET: api/<ProdutoController>
        [HttpGet]
        public IActionResult Get()
        {
            List<Produto> lista = new List<Produto>();

            MySqlConnection connection = new MySqlConnection("Server=localhost;Database=apimysql;User=root;password=*******;");
            MySqlCommand command = new MySqlCommand("SELECT * FROM produto;", connection);

            connection.Open();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Produto p = new Produto(reader.GetInt32("id"), reader.GetString("nome"), reader.GetDouble("preco"), reader.GetInt32("quantidade"));
                lista.Add(p);
            }
            connection.Close();
            return Ok(lista);
        }

        // GET api/<ProdutoController>/5
        [HttpGet("{id}")]
        public Produto Get(int id)
        {
            Produto p = new Produto();

            MySqlConnection connection = new MySqlConnection("Server=localhost;Database=apimysql;User=root;password=*******;");
            MySqlCommand command = new MySqlCommand("SELECT * FROM produto WHERE id = @id;", connection);
            command.Parameters.AddWithValue("@id", id);

            connection.Open();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                p = new Produto(reader.GetInt32("id"), reader.GetString("nome"), reader.GetDouble("preco"), reader.GetInt32("quantidade"));
            }
            connection.Close();
            return p;
        }

        // POST api/<ProdutoController>
        [HttpPost]
        [Route("/api/[Controller]/cadastrar")]
        public IActionResult Post([FromBody] Produto produto)
        {
            MySqlConnection connection = new MySqlConnection("Server=localhost;Database=apimysql;User=root;password=******;");
            MySqlCommand command = new MySqlCommand("INSERT INTO produto (nome, preco, quantidade) VALUES (@n, @p, @q);", connection);
            command.Parameters.AddWithValue("@n", produto.nome);
            command.Parameters.AddWithValue("@p", produto.preco);
            command.Parameters.AddWithValue("@q", produto.quantidade);

            connection.Open();
            if (command.ExecuteNonQuery() != 0)
            {
                connection.Close();
                return Ok(produto);
            }
            else
            {
                connection.Close();
                return NoContent();
            }

        }
    }
}
