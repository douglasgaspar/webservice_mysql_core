﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace webservicemysql.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagemController : ControllerBase
    {
        [HttpPost]
        [Route("/api/[Controller]/cadastrar")]
        //Método que irá receber um objeto "Imagem" através do body e enviar seus dados para a tabela
        public IActionResult cadastrar([FromBody] Imagem imagem)
        {
            //Conexão com o servidor MySQL
            MySqlConnection connection = new MySqlConnection("Server=localhost;Database=apimysql;User=root;password=*******;");
            //Comando de INSERT na tabela "imagens"
            MySqlCommand command = new MySqlCommand("INSERT INTO imagens (imagembase64) VALUES (@base64);", connection);
            //Passando o valor da imagem já no padrão Base64 para o parâmetro
            command.Parameters.AddWithValue("@base64", imagem.imagemTexto);

            try{
                connection.Open(); //Abre a conexão

                if (command.ExecuteNonQuery() != 0){
                    //Caso o comando INSERT seja executado com sucesso
                    return Ok(imagem); //Retorna código 200
                }else{
                    return BadRequest(); //Retorna código 400
                }
            }
            catch (Exception exc){
                return StatusCode(500); //Retorna código 500
                throw new Exception(exc.ToString());
            }finally{
                connection.Close(); //Fecha a conexão
            }
           
        }

        [HttpGet]
        [Route("/api/[Controller]/buscarTodos")]
        public IActionResult buscarTodos(){
            //Lista que será populada com as imagens do banco de dados
            List<Imagem> list = new List<Imagem>();
            
            //Conexão com o banco de dados MySQL
            MySqlConnection connection = new MySqlConnection("Server=localhost;Database=apimysql;User=root;password=*******;");
            MySqlCommand command = new MySqlCommand("SELECT * FROM imagens;", connection);

            try{
                connection.Open(); //Abre a conexão
                MySqlDataReader reader = command.ExecuteReader(); //Executa o comando SELECT no MySQL

                while (reader.Read()){ //Percorre todos os dados retornados pelo SELECT
                    //Cria um objeto da classe Imagem
                    Imagem imagem = new Imagem(reader.GetInt32("id"), reader.GetString("imagemBase64"));
                    list.Add(imagem); //Adiciona cada imagem na lista
                }
            }
            catch (Exception exc){
                return StatusCode(500); //Retorna código 500
                throw new Exception(exc.ToString());
            }finally{
                connection.Close(); //Fecha a conexão
            }

            
            return Ok(list); //Retorna código 200
        }
    }
}
