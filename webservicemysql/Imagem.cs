﻿namespace webservicemysql
{
    public class Imagem
    {
        public int id { get; set; }
        //public byte[] imagemByte { get; set; }
        public String imagemTexto { get; set; }

        public Imagem() { }

        public Imagem(int id, string imagemTexto)
        {
            this.id = id;
            this.imagemTexto = imagemTexto;
        }
    }
}
